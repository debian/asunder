# Swedish translation for Asunder.
# This file is distributed under the same license as the Asunder package.
# Copyright (C) 2007, 2009 Free Software Foundation, Inc.
# Daniel Nylander <po@danielnylander.se>, 2007, 2009.
# Andreas Rönnquist <andreas@ronnquist.net> 2012-2018
#
msgid ""
msgstr ""
"Project-Id-Version: asunder\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-21 21:44-0400\n"
"PO-Revision-Date: 2018-04-07 14:29+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural= n!=1;\n"
"X-Generator: Poedit 1.8.11\n"

#: ../src/main.c:146 ../src/interface.c:314
msgid "Rip"
msgstr "Extrahera"

#: ../src/main.c:156
msgid "Track"
msgstr "Spår"

#: ../src/main.c:164
msgid "Artist"
msgstr "Artist"

#: ../src/main.c:172 ../src/main.c:180
msgid "Title"
msgstr "Titel"

#: ../src/main.c:188
msgid "Time"
msgstr "Tid"

#: ../src/main.c:217
msgid ""
"'cdparanoia' was not found in your path. Asunder requires cdparanoia to rip "
"CDs."
msgstr ""
"\"cdparanoia\" hittades inte i dina sökvägar. Asunder kräver cdparanoia för "
"att extrahera cd-skivor."

#. Debug print add time last taken by the open() call.
#. snprintf(msgStr, 1024, "%s [open %.1lf sec]",
#. _("<b>Checking disc...</b>"), open_diff_time / 1E6);
#. This is to deal with the fact that the signal below will take up to
#. a second to get processed in refresh_thread() which will call check_disk()
#. which will set the label to the same thing at the start.
#: ../src/main.c:267 ../src/main.c:292 ../src/main.c:957
msgid "<b>Checking disc...</b>"
msgstr "<b>Kontrollerar skiva...</b>"

#: ../src/main.c:551
msgid "<b>Getting disc info from the internet...</b>"
msgstr "<b>Hämtar skivinformation från Internet...</b>"

#: ../src/main.c:668
msgid "<b>Reading disc...</b>"
msgstr "<b>Läser skiva...</b>"

#: ../src/callbacks.c:60 ../src/callbacks.c:294 ../src/callbacks.c:322
#: ../src/callbacks.c:331 ../src/callbacks.c:340 ../src/callbacks.c:350
#: ../src/interface.c:725 ../src/interface.c:812 ../src/interface.c:921
#: ../src/interface.c:1057
#, c-format
msgid "%dKbps"
msgstr "%dKbps"

#: ../src/callbacks.c:737
msgid "No CD is inserted. Please insert a CD into the CD-ROM drive."
msgstr "Ingen skiva är inmatad. Mata in en skiva i cd-rom-enheten."

#: ../src/callbacks.c:756 ../src/callbacks.c:782 ../src/callbacks.c:807
#: ../src/callbacks.c:832 ../src/callbacks.c:857 ../src/callbacks.c:883
#: ../src/callbacks.c:908 ../src/callbacks.c:933
#, c-format
msgid ""
"%s was not found in your path. Asunder requires it to create %s files. All "
"%s functionality is disabled."
msgstr ""
"%s hittades inte i dina sökvägar. Asunder kräver det för att skapa %s-filer. "
"All %s-funktionalitet har inaktiverats."

#: ../src/callbacks.c:1023
msgid "Select all for ripping"
msgstr "Markera alla för extrahering"

#: ../src/callbacks.c:1029
msgid "Deselect all for ripping"
msgstr "Avmarkera alla för extrahering"

#: ../src/callbacks.c:1035
msgid "Capitalize Artists & Titles"
msgstr "Använd stor bokstav i Artist & Titel"

#: ../src/callbacks.c:1065
msgid "Swap Artist <=> Title"
msgstr "Växla Artist <=> Titel"

#: ../src/interface.c:98
msgid "CDDB Lookup"
msgstr "CDDB-uppslag"

#: ../src/interface.c:161
msgid "Disc:"
msgstr "Skiva:"

#. 2nd the labels in col 1:
#: ../src/interface.c:168
msgid "Album Artist:"
msgstr "Albumartist:"

#: ../src/interface.c:175
msgid "Album Title:"
msgstr "Albumtitel:"

#: ../src/interface.c:182
msgid "Single Artist"
msgstr "En Artist"

#: ../src/interface.c:195
msgid "First track number:"
msgstr "Första spårnummer:"

#: ../src/interface.c:208
msgid "Track number width in filename:"
msgstr "Bredd på spårnummer i filnamn:"

#. Category
#: ../src/interface.c:235
msgid "Category:"
msgstr ""

#. Genre and Year
#: ../src/interface.c:243
msgid "Genre / Year:"
msgstr "Genre / År:"

#: ../src/interface.c:476
msgid "Preferences"
msgstr "Inställningar"

#: ../src/interface.c:493 ../src/interface.c:499
msgid "Destination folder"
msgstr "Målmapp"

#: ../src/interface.c:503
msgid "Create M3U playlist"
msgstr "Skapa M3U-spellista"

#: ../src/interface.c:511
msgid "CD-ROM device: "
msgstr "Cd-rom-enhet: "

#: ../src/interface.c:520
msgid ""
"Default: /dev/cdrom\n"
"Other example: /dev/hdc\n"
"Other example: /dev/sr0"
msgstr ""
"Standard: /dev/cdrom\n"
"Annat exempel: /dev/hdc\n"
"Annat exempel: /dev/sr0"

#: ../src/interface.c:524
msgid "Eject disc when finished"
msgstr "Mata ut skivan när den är färdig"

#: ../src/interface.c:532
msgid "General"
msgstr "Allmänt"

#: ../src/interface.c:552
msgid ""
"%A - Artist\n"
"%L - Album\n"
"%N - Track number (2-digit)\n"
"%Y - Year (4-digit or \"0\")\n"
"%T - Song title"
msgstr ""
"%A - Artist\n"
"%L - Album\n"
"%N - Spårnummer (2-siffrigt)\n"
"%Y - Årtal (4-siffrigt eller \"0\")\n"
"%T - Låttitel"

#: ../src/interface.c:557
#, c-format
msgid "%G - Genre"
msgstr "%G - Genre"

#: ../src/interface.c:572
msgid "Album directory: "
msgstr "Albumkatalog: "

#: ../src/interface.c:579 ../src/prefs.c:776
msgid "Playlist file: "
msgstr "Spellistfil: "

#: ../src/interface.c:586 ../src/prefs.c:788 ../src/prefs.c:798
msgid "Music file: "
msgstr "Musikfil: "

#: ../src/interface.c:600
msgid ""
"This is relative to the destination folder (from the General tab).\n"
"Can be blank.\n"
"Default: %A - %L\n"
"Other example: %A/%L"
msgstr ""
"Det här är relativt till målmappen (från fliken Allmänt).\n"
"Kan vara blank.\n"
"Standard: %A - %L\n"
"Annat exempel: %A/%L"

#: ../src/interface.c:612
msgid ""
"This will be stored in the album directory.\n"
"Can be blank.\n"
"Default: %A - %L"
msgstr ""
"Det här kommer att lagras i albumkatalogen.\n"
"Kan vara blank.\n"
"Standard: %A - %L"

#: ../src/interface.c:623
msgid ""
"This will be stored in the album directory.\n"
"Cannot be blank.\n"
"Default: %A - %T\n"
"Other example: %N - %T"
msgstr ""
"Det här kommer att lagras i albumkatalogen.\n"
"Får inte vara blank.\n"
"Standard: %A - %T\n"
"Annat exempel: %N - %T"

#: ../src/interface.c:628
msgid "Filename formats"
msgstr "Filnamnsformat"

#: ../src/interface.c:633
msgid "Allow changing first track number"
msgstr "Tillåt att ändra första spårnummer"

#: ../src/interface.c:638
msgid "Filenames"
msgstr "Filnamn"

#. WAV
#. frame3 = gtk_frame_new (NULL);
#. gtk_frame_set_shadow_type(GTK_FRAME(frame3), GTK_SHADOW_IN);
#. gtk_widget_show (frame3);
#. gtk_box_pack_start (GTK_BOX (vbox), frame3, FALSE, FALSE, 0);
#. alignment8 = gtk_alignment_new (0.5, 0.5, 1, 1);
#. gtk_widget_show (alignment8);
#. gtk_container_add (GTK_CONTAINER (frame3), alignment8);
#. gtk_alignment_set_padding (GTK_ALIGNMENT (alignment8), 2, 2, 12, 2);
#. vbox2 = gtk_vbox_new (FALSE, 0);
#. gtk_widget_show (vbox2);
#. gtk_container_add (GTK_CONTAINER (alignment8), vbox2);
#: ../src/interface.c:672
msgid "WAV (uncompressed)"
msgstr "WAV (okomprimerad)"

#: ../src/interface.c:693
msgid "Variable bit rate (VBR)"
msgstr "Variabel bitfrekvens (VBR)"

#: ../src/interface.c:701
msgid "Better quality for the same size."
msgstr "Bättre kvalitet med samma storlek."

#: ../src/interface.c:707 ../src/interface.c:794 ../src/interface.c:905
#: ../src/interface.c:991 ../src/interface.c:1039
msgid "Bitrate"
msgstr "Bitfrekvens"

#: ../src/interface.c:722 ../src/interface.c:809
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"192Kbps."
msgstr ""
"Högre bitfrekvens ger bättre kvalitet men även en större fil. De flesta "
"använder 192Kbps."

#: ../src/interface.c:731
msgid "MP3 (lossy compression)"
msgstr "MP3 (förstörande komprimering)"

#: ../src/interface.c:754
msgid "Quality"
msgstr "Kvalitet"

#: ../src/interface.c:765
msgid "Higher quality means bigger file. Default is 6."
msgstr "Högre kvalitet betyder större fil. Standard är 6."

#: ../src/interface.c:767
msgid "OGG Vorbis (lossy compression)"
msgstr "OGG Vorbis (förstörande komprimering)"

#: ../src/interface.c:818
msgid "AAC (lossy compression)"
msgstr "AAC (förstörande komprimering)"

#: ../src/interface.c:841 ../src/interface.c:957 ../src/interface.c:1096
msgid "Compression level"
msgstr "Komprimeringsnivå"

#: ../src/interface.c:852 ../src/interface.c:1109
msgid "This does not affect the quality. Higher number means smaller file."
msgstr "Det här påverkar inte kvaliteten. Högre tal betyder mindre fil."

#: ../src/interface.c:854
msgid "FLAC (lossless compression)"
msgstr "FLAC (förlustfri komprimering)"

#: ../src/interface.c:873
msgid "More formats"
msgstr "Fler format"

#: ../src/interface.c:919
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"160Kbps."
msgstr ""
"Högre bitfrekvens ger bättre kvalitet men även en större fil. De flesta "
"använder 160Kbps."

#: ../src/interface.c:927
msgid "OPUS (lossy compression)"
msgstr "OPUS (förstörande komprimering)"

#: ../src/interface.c:970
msgid ""
"This does not affect the quality. Higher number means smaller file. Default "
"is 1 (recommended)."
msgstr ""
"Det här påverkar inte kvaliteten. Högre tal betyder mindre fil. Standard är "
"1 (rekommenderat)."

#: ../src/interface.c:976
msgid "Hybrid compression"
msgstr "Hybrid-komprimering"

#: ../src/interface.c:985
msgid ""
"The format is lossy but a correction file is created for restoring the "
"lossless original."
msgstr ""
"Formatet är förstörande men en korrigeringsfil skapas för att återskapa det "
"förlustfria originalet."

#: ../src/interface.c:1055
msgid "Higher bitrate is better quality but also bigger file."
msgstr "Högre bitfrekvens ger bättre kvalitet men även en större fil."

#: ../src/interface.c:1063
msgid "Musepack (lossy compression)"
msgstr "Musepack (förstörande komprimering)"

#: ../src/interface.c:1111
msgid "Monkey's Audio (lossless compression)"
msgstr "Monkey's Audio (förlustfri komprimering)"

#. END MONKEY
#. ~ expander = gtk_expander_new(_("Proprietary encoders"));
#. ~ gtk_widget_show (expander);
#. ~ gtk_box_pack_start (GTK_BOX (vbox), expander, FALSE, FALSE, 0);
#. ~ GLADE_HOOKUP_OBJECT (prefs, expander, "proprietary_formats_expander");
#. ~ hiddenbox = gtk_vbox_new (FALSE, 0);
#. ~ gtk_widget_show (hiddenbox);
#. ~ gtk_container_add (GTK_CONTAINER (expander), hiddenbox);
#: ../src/interface.c:1129
msgid "Encode"
msgstr "Kodning"

#: ../src/interface.c:1164
msgid "Get disc info from the internet automatically"
msgstr "Hämta skivinformation automatiskt från Internet"

#: ../src/interface.c:1173 ../src/interface.c:1229
msgid "Server: "
msgstr "Server: "

#: ../src/interface.c:1183
msgid "The CDDB server to get disc info from (default is gnudb.gnudb.org)"
msgstr ""
"CDDB-servern att hämta skivinformation från (standard är gnudb.gnudb.org)"

#: ../src/interface.c:1191 ../src/interface.c:1242
msgid "Port: "
msgstr "Port: "

#: ../src/interface.c:1201
msgid "The CDDB server port (default is 8880)"
msgstr "Port för CDDB-server (standard är 8880)"

#: ../src/interface.c:1216
msgid "Use an HTTP proxy to connect to the internet"
msgstr "Använd en HTTP-proxy för att ansluta till Internet"

#: ../src/interface.c:1264
msgid "Artist/Title separator: "
msgstr ""

#: ../src/interface.c:1275
msgid "Log to /var/log/asunder.log"
msgstr "Logga till /var/log/asunder.log"

#: ../src/interface.c:1280
msgid "Faster ripping (no error correction)"
msgstr "Snabbare extrahering (ingen felkorrigering)"

#: ../src/interface.c:1289
msgid "Advanced"
msgstr "Avancerat"

#: ../src/interface.c:1362 ../src/interface.c:1398
msgid "Ripping"
msgstr "Extrahering"

#: ../src/interface.c:1391
msgid "Total progress"
msgstr "Totalt förlopp"

#: ../src/interface.c:1405
msgid "Encoding"
msgstr "Kodning"

#: ../src/interface.c:2060
msgid ""
"An application to save tracks from an Audio CD \n"
"as WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio, and/or AAC "
"files."
msgstr ""
"Ett program för att spara spår från en ljud-CD \n"
"som WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio och/eller "
"AAC-filer."

#: ../src/interface.c:2102
#, c-format
msgid "%d file created successfully"
msgid_plural "%d files created successfully"
msgstr[0] "%d fil skapades"
msgstr[1] "%d filer skapades"

#: ../src/interface.c:2111
#, c-format
msgid "There was an error creating %d file"
msgid_plural "There was an error creating %d files"
msgstr[0] "Det uppstod ett fel vid skapandet av %d fil"
msgstr[1] "Det uppstod ett fel vid skapandet av %d filer"

#: ../src/prefs.c:775 ../src/prefs.c:787
#, c-format
msgid "Invalid characters in the '%s' field"
msgstr "Ogiltiga tecken i fältet \"%s\""

#: ../src/prefs.c:797
#, c-format
msgid "'%s' cannot be empty"
msgstr "\"%s\" får inte vara tom"

#: ../src/prefs.c:812
msgid "Invalid proxy port number"
msgstr "Ogiltigt portnummer för proxy"

#: ../src/prefs.c:825
msgid "Invalid cddb server port number"
msgstr "Ogiltigt portnummer för cddb-server"

#: ../src/support.c:47
msgid "Overwrite?"
msgstr "Skriv över?"

#: ../src/support.c:60
#, c-format
msgid "The file '%s' already exists. Do you want to overwrite it?\n"
msgstr "Filen \"%s\" finns redan. Vill du skriva över den?\n"

#: ../src/support.c:66
msgid "Remember the answer for _all the files made from this CD"
msgstr "Kom ihåg svaret för _alla filer skapade från denna skiva"

#: ../src/threads.c:189
msgid ""
"No ripping/encoding method selected. Please enable one from the "
"'Preferences' menu."
msgstr ""
"Ingen metod för extrahering/kodning har valts. Aktivera en från menyn "
"\"Inställningar\"."

#: ../src/threads.c:221
msgid ""
"No tracks were selected for ripping/encoding. Please select at least one "
"track."
msgstr "Inga spår har markerats för extrahering/kodning. Välj minst ett spår."

#: ../src/threads.c:1299 ../src/threads.c:1301 ../src/threads.c:1305
msgid "Waiting..."
msgstr "Väntar..."

#~ msgid "Split 'Artist/Title' in Title field"
#~ msgstr "Dela 'Artist/Titel' i titelfältet"

#~ msgid "Proprietary encoders"
#~ msgstr "Proprietära kodare"

#~ msgid "Higher quality means bigger file. Default is 60."
#~ msgstr "Högre kvalitet betyder större fil. Standard är 60."

#~ msgid "AAC (lossy compression, Nero encoder)"
#~ msgstr "AAC (förstörande komprimering, Nero-kodare)"

#~ msgid "Genre"
#~ msgstr "Genre"

#~ msgid "Single Genre"
#~ msgstr "En Genre"

#, fuzzy
#~ msgid ""
#~ "%s was not found in your path. Asunder requires it to create %s files.All "
#~ "%s functionality is disabled."
#~ msgstr ""
#~ "%s hittades inte i dina sökvägar. Asunder kräver det för att skapa %s-"
#~ "filer. All %s-funktionalitet har inaktiverats."

#, fuzzy
#~ msgid "Playlist file"
#~ msgstr "Spellistfil: "

#, fuzzy
#~ msgid "Music file"
#~ msgstr "Musikfil: "

#~ msgid "Create directory for each album"
#~ msgstr "Skapa katalog för varje album"

#~ msgid "These characters will be removed from all filenames."
#~ msgstr "Dessa tecken kommer att tas bort från alla filnamn."
